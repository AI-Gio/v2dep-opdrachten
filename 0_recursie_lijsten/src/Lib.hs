module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- Schrijf een functie die de som van een lijst getallen berekent, net als de `product` functie uit de les.
-- | Berekent de som van een lijst met integers
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 list = head list + ex1 (tail list)

-- Schrijf een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Verhoogt elk element(Int) in een lijst met 1
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1 : ex2 (xs)

-- Schrijf een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Vermenigvuldigt elk element(Int) in een lijst met -1
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1) : ex3 (xs)

-- Schrijf een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6]. Maak hierbij geen gebruik van de standaard-functies, maar los het probleem zelf met (expliciete) recursie op. Hint: je hoeft maar door een van beide lijsten heen te lopen met recursie.
-- | Concateneert 2 lijsten met integers aan elkaar met behulp van recursie
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] lijst2 = lijst2
ex4 (x:xs) lijst2 = x : (ex4 xs lijst2)

-- Schrijf een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Telt paarsgewijs 2 lijsten met integers bij elkaar op
ex5 :: [Int] -> [Int] -> [Int]
ex5 [] [] = []
ex5 (x:xs) (y:ys) = x + y : (ex5 xs ys)

-- Schrijf een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18]. Het is voor deze opgave nog niet de bedoeling dat je hogere-orde functies gebruikt.
-- | Vermenigvuldigt 2 lijsten met integers paarsgewijs
ex6 :: [Int] -> [Int] -> [Int]
ex6 [] [] = []
ex6 (x:xs) (y:ys) = x * y : (ex6 xs ys)

<<<<<<< HEAD
-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig prodct uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
-- | Vermenigvuldigt 2 lijsten met integers paarsgewijs en telt elk element bij elkaar op en geeft een integers terug
=======
-- Schrijf een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
>>>>>>> 4c601b82a933e8bebb1a57804a9574dadc581a39
ex7 :: [Int] -> [Int] -> Int
ex7 [] [] = 0
ex7 (x:xs) (y:ys) = x * y + (ex7 xs ys)
